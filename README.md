# Mathematik V ZF
[![CC BY-SA 3.0][cc-by-sa-shield]][cc-by-sa]

Zusammenfassung für die Vorlesung [*Mathematik V*](http://www.vvz.ethz.ch/Vorlesungsverzeichnis/lerneinheit.view?lerneinheitId=150657&semkez=2021S&ansicht=LEHRVERANSTALTUNGEN&lang=de) bei M. A. Sprenger und weitere Dozenten im FS21.

## Kompiliertes `.pdf`
Findest du hier: https://n.ethz.ch/~jannisp/download/Mathematik-V/

Vorschau-PDF werden automatisch unter https://server.thisfro.ch/download/latex-previews/ veröffentlicht.  
:warning: Achtung: Diese sind meist noch nicht fertig!

## Änderungen
Falls du irgendwelche Fehler findest oder Sache ergänzen willst, darfst du die gerne selbst korrigieren/einfügen und einen Pull request öffnen. Ansonsten kontaktiere mich direkt ([jannisp](jannispmailto:jannisp@student.ethz.ch)).

## Copyleft
Ausser den Grafiken unterliegt die Zusammenfassung der [Creative Commons Attribution-ShareAlike 3.0 International License][cc-by-sa].

[![CC BY-SA 3.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/3.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/3.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%203.0-lightgrey.svg

